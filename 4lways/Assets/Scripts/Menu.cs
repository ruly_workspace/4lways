﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    bool interacting = true;
    int _position = 1;
    bool izquierda = false;
    bool creditsOpen = false;
    [SerializeField] private AudioClip introSong;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("WaitInteraction", 2f);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetAxisRaw("Horizontal") > 0 && !interacting && !creditsOpen)
        {
            if(_position == 0)
            {
                izquierda = false;
            }
            // mover botón a la derecha if possible
            if(_position < 2)
            {
                _position++;
                MoveCursor(_position, izquierda);
                interacting = true;
                StartCoroutine("WaitInteraction", 0.5f);
            }
        }
        if (Input.GetAxisRaw("Horizontal") < 0 && !interacting && !creditsOpen)
        {
            if (_position == 2)
            {
                izquierda = true;
            }
            // mover botón a la derecha if possible
            if (_position > 0)
            {
                _position--;
                MoveCursor(_position, izquierda);
                interacting = true;
                StartCoroutine("WaitInteraction", 0.5f);
            }
        }
        if(Input.GetKeyDown("space"))
        {
            Interact(_position);
            interacting = true;
            StartCoroutine("WaitInteraction", 2f);
        }
    }

    IEnumerator WaitInteraction(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        interacting = false;
    }

    IEnumerator StartGame()
    {
        GetComponent<AudioSource>().clip = introSong;
        GetComponent<AudioSource>().Play();
        StartCoroutine("FadeIn");
        yield return new WaitForSeconds(3f);
        interacting = true;
        yield return new WaitForSeconds(34f);
        SceneManager.LoadScene("MainScene");
    }

    void MoveCursor(int position, bool izquierda)
    {
        if(position == 1 && izquierda)
        {
            GetComponent<Animator>().Play("Go_Exit_Play", 0, 0f);
        }
        if (position == 1 && !izquierda)
        {
            GetComponent<Animator>().Play("Go_Credits_Play", 0, 0f);
        } if(position == 2)
        {
            GetComponent<Animator>().Play("Go_Play_Exit", 0, 0f);
        } if (position == 0)
        {
            GetComponent<Animator>().Play("Go_Play_Credits", 0, 0f);
        }
    }

    void Interact(int position)
    {
        switch(position)
        {
            case 0: // creditos
                _position = 3;
                Debug.Log(position);
                creditsOpen = true;
                GetComponent<Animator>().Play("Open_Credits", 0, 0f);
                break;
            case 1: // jugar
                GetComponent<Animator>().Play("Intro_Cinematic", 0, 0f);
                StartCoroutine("FadeOut");
                StartCoroutine("StartGame");
                break;
            case 2: // salir
                Application.Quit();
                break;
            case 3: // salir de créditos;
                _position = 0;
                creditsOpen = false;
                GetComponent<Animator>().Play("Close_Credits", 0, 0f);
                break;
        }
    }

    IEnumerator FadeIn()
    {
        float speed = 0.01f;

        for (float i = 0; i < 1; i += speed)
        {
            GetComponent<AudioSource>().volume = i;
            yield return null;
        }
    }

    IEnumerator FadeOut()
    {
        float speed = 0.01f;

        for (float i = 1; i > 0; i += speed)
        {
            GetComponent<AudioSource>().volume = i;
            yield return null;
        }
    }
}
