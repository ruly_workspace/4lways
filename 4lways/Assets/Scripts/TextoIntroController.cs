﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class TextoIntroController : MonoBehaviour
{
    private TMP_Text m_TextComponent;

    public bool fading = false;
    public bool fadingIn = false;

    public float timeFadeIn = 1;
    public float timeTillFadeOut = 2f;
    public float timeFadeOut = 1f;

    float deltaReferenceTime
    {
        get
        {
            return fadingIn ? timeFadeIn : timeFadeOut;
        }
    }

    float delta = 0;

    float deltaFade
    {
        get
        {
            return delta;
        }
        set
        {
            delta = Mathf.Clamp(value, 0f, deltaReferenceTime);
        }
    }

    void Awake()
    {
        m_TextComponent = GetComponentInChildren<TMP_Text>();
    }

    void OnEnable()
    {
        StartCoroutine(AnimateVertexColors());
        StartCoroutine(AnimateText());
    }

    void OnDisable()
    {
        StopAllCoroutines();
    }

    IEnumerator AnimateText()
    {
        FadeIn(true);
        yield return new WaitForSeconds(timeFadeIn);
        fading = false;
        PaintAllBase();
        yield return new WaitForSeconds(timeTillFadeOut);
        deltaFade = deltaReferenceTime;
        FadeIn(false);
        yield return new WaitForSeconds(timeFadeOut);
        PaintAllBase();
    }

    int par = 0;

    /// <summary>
    /// Method to animate vertex colors of a TMP Text object.
    /// </summary>
    /// <returns></returns>
    IEnumerator AnimateVertexColors()
    {
        // Force the text object to update right away so we can have geometry to modify right from the start.
        m_TextComponent.ForceMeshUpdate();

        TMP_TextInfo textInfo = m_TextComponent.textInfo;
        int currentCharacter = 0;

        Color32[] newVertexColors;
        Color32 c0 = m_TextComponent.color;

        while (true)
        {
            if (fading)
            {
                int characterCount = textInfo.characterCount;

                // If No Characters then just yield and wait for some text to be added
                if (characterCount == 0)
                {
                    yield return new WaitForSeconds(0.25f);
                    continue;
                }

                // Get the index of the material used by the current character.
                int materialIndex = textInfo.characterInfo[currentCharacter].materialReferenceIndex;

                // Get the vertex colors of the mesh used by this text element (character or sprite).
                newVertexColors = textInfo.meshInfo[materialIndex].colors32;

                // Get the index of the first vertex used by this text element.
                int vertexIndex = textInfo.characterInfo[currentCharacter].vertexIndex;

                // Only change the vertex color if the text element is visible.
                if (textInfo.characterInfo[currentCharacter].isVisible)
                {
                    if (fadingIn)
                    {
                        deltaFade += Time.deltaTime;
                        UpdateColorOpacity(ref c0);
                    }
                    else
                    {
                        deltaFade -= Time.deltaTime;
                        UpdateColorOpacity(ref c0);
                    }

                    newVertexColors[vertexIndex + 0] = c0;
                    newVertexColors[vertexIndex + 1] = c0;
                    newVertexColors[vertexIndex + 2] = c0;
                    newVertexColors[vertexIndex + 3] = c0;

                    // New function which pushes (all) updated vertex data to the appropriate meshes when using either the Mesh Renderer or CanvasRenderer.
                    m_TextComponent.UpdateVertexData(TMP_VertexDataUpdateFlags.Colors32);

                    // This last process could be done to only update the vertex data that has changed as opposed to all of the vertex data but it would require extra steps and knowing what type of renderer is used.
                    // These extra steps would be a performance optimization but it is unlikely that such optimization will be necessary.
                }

                currentCharacter = (currentCharacter + UnityEngine.Random.Range(0, characterCount)) % characterCount;
            }
            if (par == 1)
            {
                yield return null;
            }
            par = par == 1 ? 0 : 1;
        }
    }

    void PaintAllBase()
    {
        TMP_TextInfo textInfo = m_TextComponent.textInfo;
        int currentCharacter = 0;

        Color32[] newVertexColors;
        Color32 c0 = m_TextComponent.color;

        int characterCount = textInfo.characterCount;

        while (currentCharacter < characterCount)
        {
            // Get the index of the material used by the current character.
            int materialIndex = textInfo.characterInfo[currentCharacter].materialReferenceIndex;

            // Get the vertex colors of the mesh used by this text element (character or sprite).
            newVertexColors = textInfo.meshInfo[materialIndex].colors32;

            // Get the index of the first vertex used by this text element.
            int vertexIndex = textInfo.characterInfo[currentCharacter].vertexIndex;

            // Only change the vertex color if the text element is visible.
            if (textInfo.characterInfo[currentCharacter].isVisible)
            {
                Color32 auxColor = new Color32(c0.r, c0.g, c0.b, Convert.ToByte(255 * (fadingIn ? 1 : 0)));
                newVertexColors[vertexIndex + 0] = auxColor;
                newVertexColors[vertexIndex + 1] = auxColor;
                newVertexColors[vertexIndex + 2] = auxColor;
                newVertexColors[vertexIndex + 3] = auxColor;

                // New function which pushes (all) updated vertex data to the appropriate meshes when using either the Mesh Renderer or CanvasRenderer.
                m_TextComponent.UpdateVertexData(TMP_VertexDataUpdateFlags.Colors32);
            }

            currentCharacter++;
        }
    }

    void UpdateColorOpacity(ref Color32 color)
    {
        color = new Color32(color.r, color.g, color.b, Convert.ToByte(255 * (deltaFade / deltaReferenceTime)));
    }

    public void FadeIn(bool fadeIn)
    {
        fadingIn = fadeIn;
        fading = true;
    }
}
