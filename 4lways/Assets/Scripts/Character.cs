﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    private Rigidbody2D body;
    private TextObject textObject;

    private float horizontal;
    private float vertical;
    private float moveLimiter = 0.7f;

    public float runSpeed = 20.0f;

    [SerializeField] string charName;
    public bool colliding = false;
    public bool isFused = false;

    public bool complete = false;

    [SerializeField] private RuntimeAnimatorController[] animators;

    /*
     * 0 - Valor
     * 1 - Sabiduría
     * 2 - Empatía
     * 3 - Carisma
     * 4 - Valor + Sabiduría
     * 5 - Valor + Empatía
     * 6 - Valor + Carisma
     * 7 - Sabiduría + Empatía
     * 8 - Sabiduría + Carisma
     * 9 - Empatía + Carisma
     * 10 - Valor + Sabiduría + Empatía
     * 11 - Valor + Sabiduría + Carisma
     * 12 - Valor + Empatía + Carisma
     * 13 - Sabiduría + Empatía + Carisma
     * 14 - Todos
     */ 

    public ColorsData colorsData;

    void Awake()
    {
        body = GetComponent<Rigidbody2D>();
        textObject = GetComponentInChildren<TextObject>();
        textObject.gameObject.SetActive(false);
    }

    void Update()
    {
        // Gives a value between -1 and 1
        horizontal = Input.GetAxisRaw("Horizontal"); // -1 is left
        vertical = Input.GetAxisRaw("Vertical"); // -1 is down
    }

    void FixedUpdate()
    {
        if (horizontal != 0 || vertical != 0) // Check for diagonal movement
        {
            // limit movement speed diagonally, so you move at 70% speed
            horizontal *= moveLimiter;
            vertical *= moveLimiter;
            if (horizontal > 0)
            {
                this.GetComponent<SpriteRenderer>().flipX = true;
            } else if(horizontal <= 0)
            {
                this.GetComponent<SpriteRenderer>().flipX = false;
            }
            if(GetComponent<Animator>() != null)
                this.GetComponent<Animator>().SetBool("Running", true);

            
        } else
        {
            if (GetComponent<Animator>() != null)
                this.GetComponent<Animator>().SetBool("Running", false);
        }
        body.velocity = new Vector2(horizontal * runSpeed, vertical * runSpeed);

    }

    public void OnCollisionEnter2D(Collision2D collision) // fusión
    {
        
        Character fused;
        if (collision.gameObject.tag == "Character")
        {
            
            StartCoroutine("FadeIn");
            

            if (!colliding)
            {
                fused = collision.gameObject.GetComponent<Character>();
                fused.colliding = true;
                charName = charName + "," + fused.charName;

                // cambiar sprite del superviviente por la mezcla de los dos
                List<string> tokenA = new List<string>(charName.Split(','));
                List<string> tokenB = new List<string>(fused.charName.Split(','));

                bool isValor = false;
                bool isWisdom = false;
                bool isEmpathy = false;
                bool isCharisma = false;

                if (tokenA.Contains("valor") || tokenB.Contains("valor"))
                {
                    isValor = true;
                    Debug.Log("V");
                }
                if (tokenA.Contains("wisdom") || tokenB.Contains("wisdom"))
                {
                    isWisdom = true;
                    Debug.Log("W");
                }
                if (tokenA.Contains("empathy") || tokenB.Contains("empathy"))
                {
                    isEmpathy = true;
                    Debug.Log("E");
                }
                if (tokenA.Contains("charisma") || tokenB.Contains("charisma"))
                {
                    isCharisma = true;
                    Debug.Log("C");
                }

                // Perdón.
                if(isValor) // Valor
                {
                    if(isWisdom) // Valor + Sabiduría
                    {
                        if(isEmpathy) // Valor + Sabiduría + Empatía
                        {
                            if(isCharisma) // Valor + Sabiduría + Empatía + Carisma
                            {
                                this.GetComponent<Animator>().runtimeAnimatorController = animators[14];
                            } else
                            {
                                this.GetComponent<Animator>().runtimeAnimatorController = animators[10];
                            }
                        } else if(isCharisma) // Valor + Sabiduría + Carisma
                        {
                            this.GetComponent<Animator>().runtimeAnimatorController = animators[11];
                        } else // Valor + Sabiduría
                        {
                            this.GetComponent<Animator>().runtimeAnimatorController = animators[4];
                        }
                    } else if(isEmpathy) // Valor + Empatía
                    {
                        if(isCharisma) // Valor + Empatía + Carisma
                        {
                            this.GetComponent<Animator>().runtimeAnimatorController = animators[12];
                        } else
                        {
                            this.GetComponent<Animator>().runtimeAnimatorController = animators[5];
                        }
                    } else if(isCharisma) // Valor + Carisma
                    {
                        this.GetComponent<Animator>().runtimeAnimatorController = animators[6];
                    }
                } else if(isWisdom) // Sabiduría
                {
                    if(isEmpathy) // Sabiduría + Empatía
                    {
                        if(isCharisma) // Sabiduría + Empatía + Carisma
                        {
                            this.GetComponent<Animator>().runtimeAnimatorController = animators[13];
                        } else
                        {
                            this.GetComponent<Animator>().runtimeAnimatorController = animators[7];
                            Debug.Log("Está entrando al bueno");
                        }
                    } else // Sabiduría + Carisma
                    {
                        this.GetComponent<Animator>().runtimeAnimatorController = animators[8];
                    }
                } else // Empatía + Carisma
                {
                    this.GetComponent<Animator>().runtimeAnimatorController = animators[9];
                }
                // Fin del perdón.

                isFused = true;
                SortName();
                GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().ChangeCamera(this.gameObject, fused.gameObject);

                fused.GetComponent<SpriteRenderer>().enabled = false;
                fused.GetComponent<Collider2D>().enabled = false;
                fused.GetComponent<Character>().runSpeed = 0f;
            }
        }
    }

    IEnumerator FadeIn()
    {
        float speed = 0.001f;

        for (float i = 0; i < 1; i += speed)
        {
            GetComponent<AudioSource>().volume = i;
            yield return null;
        }
    }

    public void ActivateText(List<TextName> textNames)
    {
        TextName textName = textNames.Find(x => x.name == charName);
        textObject.gameObject.SetActive(true);
        textObject.ActivateText(textName != null ? textName.text : "Me huelen las manos a mandarina", colorsData.colorBase, GetColorsCharacter());
    }

    public void DeactivateText()
    {
        textObject.DeactivateText();
    }

    List<Color32> GetColorsCharacter()
    {
        return colorsData.colorClothes.Find(x => x.name == charName).colors;
    }

    void SortName()
    {
        List<string> token = new List<string>(charName.Split(','));
        if(token.Contains("valor"))
        {
            charName = "valor"; // valor
            if (token.Contains("wisdom"))
            {
                charName += ",wisdom"; // valor,wisdom
                if(token.Contains("empathy"))
                {
                    charName += ",empathy"; // valor,wisdom,empathy
                    if(token.Contains("charisma"))
                    {
                        charName += ",charisma"; // valor,wisdom,empathy,charisma
                        complete = true;
                    }
                } else
                {
                    charName += ",charisma"; // valor,wisdom,charisma
                }
            } else if (token.Contains("empathy")) {
                charName += ",empathy"; // valor,empathy
                if(token.Contains("charisma"))
                {
                    charName += ",charisma"; // valor,empathy,charisma
                }
            } else if (token.Contains("charisma"))
            {
                charName += ",charisma"; // valor,charisma
            }
        } else if(token.Contains("wisdom"))
        {
            charName = "wisdom"; // wisdom
            if(token.Contains("empathy"))
            {
                charName += ",empathy"; // wisdom,empathy
                if(token.Contains("charisma"))
                {
                    charName += ",charisma"; // wisdom,empathy,charisma
                }
            } else if (token.Contains("charisma"))
            {
                charName += ",charisma"; // wisdom,charisma
            }
        } else if(token.Contains("empathy"))
        {
            charName = "empathy"; // empathy
            if (token.Contains("charisma"))
            {
                charName += ",charisma"; // empathy,charisma
            }
        } else if(token.Contains("charisma"))
        {
            charName = "charisma"; // charisma
        }
    }
}
