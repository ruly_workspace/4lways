﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextObject : MonoBehaviour
{
    TextColorChange textColorChange;
    TMP_Text tmpText;


    private void Awake()
    {
        textColorChange = GetComponent<TextColorChange>();
        tmpText = GetComponentInChildren<TMP_Text>();
    }

    public void ActivateText(string text, Color32 colorBase, List<Color32> colorsExtra)
    {
        tmpText.text = text;
        textColorChange.FadeIn(true);
        textColorChange.SetColors(colorBase, colorsExtra);
        textColorChange.StartAnimation();
        // Fade in, cambio de texto y empieza animarse
    }

    public void DeactivateText()
    {
        textColorChange.FadeIn(false);
    }

    public void EndFadeOut ()
    {
        textColorChange.StopAnimation();
        tmpText.text = "";
    }

}
