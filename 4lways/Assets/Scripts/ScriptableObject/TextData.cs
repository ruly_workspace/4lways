﻿using UnityEngine;
using System;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/TextData", order = 0)]
public class TextData : ScriptableObject
{
    [SerializeField]
    public List<TextName> data;
}

[Serializable]
public class TextName
{
    public string name;
    public string text;
}
