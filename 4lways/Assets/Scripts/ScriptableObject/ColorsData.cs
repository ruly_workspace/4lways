﻿using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/ColorsData", order = 1)]
public class ColorsData : ScriptableObject
{
    public Color32 colorBase;
    public List<ColorClothes> colorClothes;
}

[Serializable]
public class ColorClothes
{
    public string name;
    [SerializeField]
    public List<Color32> colors;
}
