﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteLayerModifier : MonoBehaviour
{
    SpriteRenderer spriteRenderer;
    Transform _transform;

    int lastPosY = 0;

    public bool staticSprite = false;
    public float offsetY = 0;

    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        _transform = transform;
        lastPosY = (int)_transform.position.y;
        spriteRenderer.sortingOrder = -(int)(_transform.position.y + offsetY);
    }

    // Update is called once per frame
    void Update()
    {
        if (!staticSprite)
        {
            if (lastPosY != (int)_transform.position.y)
            {
                spriteRenderer.sortingOrder = -(int)(_transform.position.y + offsetY);
            }
            lastPosY = (int)_transform.position.y;
        }
    }
}
