﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World_End : MonoBehaviour
{
    [SerializeField] private bool vertical;
    [SerializeField] private Transform mirror;
    [SerializeField] private int edge;

    /*
     * 1- left
     * 2 - top
     * 3 - right
     * 4 - bottom
     */

    float distanceX;
    float distanceY;

    bool isRight;
    bool isTop;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnTriggerEnter2D(Collider2D collision)
    {       

        if (collision.tag == "Character")
        {
            switch (edge)
            {
                case 1:
                    collision.transform.position = new Vector3(mirror.transform.position.x - 4, collision.transform.position.y, 1f);
                    break;
                case 2:
                    collision.transform.position = new Vector3(collision.transform.position.x, mirror.transform.position.y + 4, 1f);
                    break;
                case 3:
                    collision.transform.position = new Vector3(mirror.transform.position.x + 4, collision.transform.position.y, 1f);
                    break;
                case 4:
                    collision.transform.position = new Vector3(collision.transform.position.x, mirror.transform.position.y - 4, 1f);
                    break;
                case 5: // top left
                    distanceX = Mathf.Abs(transform.position.x - collision.transform.position.x);
                    distanceY = Mathf.Abs(transform.position.y - collision.transform.position.y);

                    isRight = false;
                    isTop = false;
                    if(transform.position.x <= collision.transform.position.x)
                    {
                        isRight = true;
                    }

                    if(transform.position.y <= collision.transform.position.y)
                    {
                        isTop = true;
                    }

                    if(isRight)
                    {
                        if(isTop)
                        {
                            collision.transform.position = new Vector3(mirror.transform.position.x + distanceX + 6, mirror.transform.position.y + distanceY + 6, 1);
                        } else
                        {
                            collision.transform.position = new Vector3(mirror.transform.position.x + distanceX + 6, mirror.transform.position.y - distanceY + 6, 1);
                        }
                    } else
                    {
                        if (isTop)
                        {
                            collision.transform.position = new Vector3(mirror.transform.position.x - distanceX + 6, mirror.transform.position.y + distanceY + 6, 1);
                        }
                        else
                        {
                            collision.transform.position = new Vector3(mirror.transform.position.x - distanceX + 6, mirror.transform.position.y - distanceY + 6, 1);
                        }
                    }
                    
                    break;
                case 6: // top right
                    distanceX = Mathf.Abs(transform.position.x - collision.transform.position.x);
                    distanceY = Mathf.Abs(transform.position.y - collision.transform.position.y);

                    isRight = false;
                    isTop = false;
                    if (transform.position.x <= collision.transform.position.x)
                    {
                        isRight = true;
                    }

                    if (transform.position.y <= collision.transform.position.y)
                    {
                        isTop = true;
                    }

                    if (isRight)
                    {
                        if (isTop)
                        {
                            collision.transform.position = new Vector3(mirror.transform.position.x + distanceX, mirror.transform.position.y + distanceY, 1);
                        }
                        else
                        {
                            collision.transform.position = new Vector3(mirror.transform.position.x + distanceX, mirror.transform.position.y - distanceY, 1);
                        }
                    }
                    else
                    {
                        if (isTop)
                        {
                            collision.transform.position = new Vector3(mirror.transform.position.x - distanceX, mirror.transform.position.y + distanceY, 1);
                        }
                        else
                        {
                            collision.transform.position = new Vector3(mirror.transform.position.x - distanceX, mirror.transform.position.y - distanceY, 1);
                        }
                    }
                    break;
                case 7: // bottom right
                    distanceX = Mathf.Abs(transform.position.x - collision.transform.position.x);
                    distanceY = Mathf.Abs(transform.position.y - collision.transform.position.y);

                    isRight = false;
                    isTop = false;
                    if (transform.position.x <= collision.transform.position.x)
                    {
                        isRight = true;
                    }

                    if (transform.position.y <= collision.transform.position.y)
                    {
                        isTop = true;
                    }

                    if (isRight)
                    {
                        if (isTop)
                        {
                            collision.transform.position = new Vector3(mirror.transform.position.x + distanceX - 6, mirror.transform.position.y + distanceY - 6, 1);
                        }
                        else
                        {
                            collision.transform.position = new Vector3(mirror.transform.position.x + distanceX - 6, mirror.transform.position.y - distanceY - 6, 1);
                        }
                    }
                    else
                    {
                        if (isTop)
                        {
                            collision.transform.position = new Vector3(mirror.transform.position.x - distanceX - 6, mirror.transform.position.y + distanceY - 6, 1);
                        }
                        else
                        {
                            collision.transform.position = new Vector3(mirror.transform.position.x - distanceX - 6, mirror.transform.position.y - distanceY - 6, 1);
                        }
                    }
                    break;
                case 8: // bottom left
                    distanceX = Mathf.Abs(transform.position.x - collision.transform.position.x);
                    distanceY = Mathf.Abs(transform.position.y - collision.transform.position.y);

                    isRight = false;
                    isTop = false;
                    if (transform.position.x <= collision.transform.position.x)
                    {
                        isRight = true;
                    }

                    if (transform.position.y <= collision.transform.position.y)
                    {
                        isTop = true;
                    }

                    if (isRight)
                    {
                        if (isTop)
                        {
                            collision.transform.position = new Vector3(mirror.transform.position.x + distanceX, mirror.transform.position.y + distanceY, 1);
                        }
                        else
                        {
                            collision.transform.position = new Vector3(mirror.transform.position.x + distanceX, mirror.transform.position.y - distanceY, 1);
                        }
                    }
                    else
                    {
                        if (isTop)
                        {
                            collision.transform.position = new Vector3(mirror.transform.position.x - distanceX, mirror.transform.position.y + distanceY, 1);
                        }
                        else
                        {
                            collision.transform.position = new Vector3(mirror.transform.position.x - distanceX, mirror.transform.position.y - distanceY, 1);
                        }
                    }
                    break;
            }
        }
    }
}
