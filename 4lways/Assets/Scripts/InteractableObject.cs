﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InteractableObject : MonoBehaviour
{
    public TextData textData;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Character")
        {
            Character character = collision.gameObject.GetComponent<Character>();
            character.ActivateText(textData.data);

            // condición endgame
            if(collision.gameObject.GetComponent<Character>().complete && name == "Obelisco")
            {
                SceneManager.LoadScene("Ending");
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Character")
        {
            Character character = collision.gameObject.GetComponent<Character>();
            character.DeactivateText();
        }
    }
}
