﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] private List<GameObject> Characters;
    [SerializeField] private RawImage[] Cameras;
    /*
     * 0 - Arriba izquierda
     * 1 - Arriba derecha
     * 2 - Abajo izquierda
     * 3 - Abajo derecha
     * 4 - Split_izquierda
     * 5 - Split_derecha_0
     * 6 - Split_derecha_1
     * 7 - Izquierda
     * 8 - Derecha
     * 9 - Grande
     * 10 - Pequeña
     * 11 - Full
     */

    private IEnumerator coroutine;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeCamera(GameObject survivor, GameObject removed)
    {
        Characters.Remove(survivor);
        Characters.Remove(removed);

        Characters.Add(survivor);
        if (Characters.Count == 3) {
            // 3 personajes restantes
            // camara izquierda se amplía
            // personajes restantes van a la derecha

            coroutine = CameraSplit(0, 1, 2, 3, 4, 5, 6);
            StartCoroutine(coroutine);

            Cameras[4].texture = Characters[Characters.Count - 1].transform.GetChild(0).GetComponent<Camera>().targetTexture;
            Characters[Characters.Count - 1].transform.GetChild(0).GetComponent<Camera>().orthographicSize = 8;

            Cameras[5].texture = Characters[0].transform.GetChild(0).GetComponent<Camera>().targetTexture;
            Cameras[6].texture = Characters[1].transform.GetChild(0).GetComponent<Camera>().targetTexture;

 
        } else if (Characters.Count == 2)
        {

            if (!Characters[0].GetComponent<Character>().isFused) {
                // 3 fusionados, 1 solo
                // camara izquierda vuelve a panorámico
                // personaje restante va a cam little
                coroutine = CameraSplit(4, 5, 6, -1, 9, 10, -1);
                StartCoroutine(coroutine);

                Cameras[9].texture = Characters[Characters.Count - 1].transform.GetChild(0).GetComponent<Camera>().targetTexture;
                Characters[Characters.Count - 1].transform.GetChild(0).GetComponent<Camera>().orthographicSize = 11;
                Cameras[10].texture = Characters[0].transform.GetChild(0).GetComponent<Camera>().targetTexture;


            } else
            {
                coroutine = CameraSplit(4, 5, 6, -1, 7, 8, -1);
                StartCoroutine(coroutine);

                Cameras[7].texture = Characters[Characters.Count - 1].transform.GetChild(0).GetComponent<Camera>().targetTexture;
                Characters[Characters.Count - 1].transform.GetChild(0).GetComponent<Camera>().orthographicSize = 8;
                Cameras[8].texture = Characters[0].transform.GetChild(0).GetComponent<Camera>().targetTexture;

            }
        } else if (Characters.Count == 1)
        {
            coroutine = CameraSplit(7, 8, 9, 10, 11, -1, -1);
            StartCoroutine(coroutine);

            Characters[0].transform.GetChild(0).GetComponent<Camera>().orthographicSize = 14;
            Cameras[11].texture = Characters[0].transform.GetChild(0).GetComponent<Camera>().targetTexture;

        }
    }

    private IEnumerator CameraSplit(int disable0, int disable1, int disable2, int disable3, int enable0, int enable1, int enable2)
    {
        if(disable0 != -1)
        {
            Cameras[disable0].gameObject.transform.GetChild(0).GetComponent<Animator>().Play("Close", 0, 0f);
        }
        if (disable1 != -1)
        {
            Cameras[disable1].gameObject.transform.GetChild(0).GetComponent<Animator>().Play("Close", 0, 0f);
        }
        if (disable2 != -1)
        {
            Cameras[disable2].gameObject.transform.GetChild(0).GetComponent<Animator>().Play("Close", 0, 0f);
        }
        if (disable3 != -1)
        {
            Cameras[disable3].gameObject.transform.GetChild(0).GetComponent<Animator>().Play("Close", 0, 0f);
        }

        yield return new WaitForSeconds(2f);

        // Desactivar Cámaras
        if (disable0 != -1)
        {
            Cameras[disable0].gameObject.SetActive(false);
        }
        if (disable1 != -1)
        {
            Cameras[disable1].gameObject.SetActive(false);
        }
        if (disable2 != -1)
        {
            Cameras[disable2].gameObject.SetActive(false);
        }
        if (disable3 != -1)
        {
            Cameras[disable3].gameObject.SetActive(false);
        }

        // Activar Cámaras
        if (enable0 != -1)
        {
            Cameras[enable0].gameObject.SetActive(true);
        }
        if (enable1 != -1)
        {
            Cameras[enable1].gameObject.SetActive(true);
        }
        if (enable2 != -1)
        {
            Cameras[enable2].gameObject.SetActive(true);
        }
    }
}
